package hyperzelly

import (
	"fmt"
	"log"
	"net"
)

type Server struct {

}

func (s *Server) Serve() {
	ln, _ := net.Listen("tcp", "0.0.0.0:8080")
	for {
		conn, _ := ln.Accept()
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer func(conn net.Conn) {
		err := conn.Close()
		if err != nil {
			log.Println(fmt.Sprintf("Failed to close connection: %s", conn.RemoteAddr().String()))
		}
	}(conn)

	path, err := handleRequest(conn)
	if err != nil {
		log.Println(err)
	}
	handleResponse(conn, path)
}
