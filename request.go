package hyperzelly

import (
	"bufio"
	"errors"
	"net"
	"strings"
)

type RequestState byte
const (
	REQUEST RequestState = 0x00
	HEADERS RequestState = 0x01
	BODY RequestState = 0x02
	COMPLETE RequestState = 0x03
)

type RequestMethod string
const (
	GET RequestMethod = "GET"
)

func handleRequest(conn net.Conn) (string, error) {
	reader := bufio.NewReader(conn)

	// Setup the state machine and start it
	requestState := REQUEST
	var method string
	var path string
	var body string
	headers := map[string]string{}
	//queryString := map[string]string{}

	for {
		if requestState == COMPLETE {
			return path, nil
		}

		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		if requestState == BODY && line == "\r\n" {
			break
		}

		switch requestState {
		case REQUEST:
			method, path, _, err = getMethodAndPath(line)
			if err != nil {
				return "", err
			}

			validVerb := false
			for _, m := range []string{"GET"} {
				if method == m {
					validVerb = true
					break
				}
			}
			if !validVerb { return "", errors.New("Invalid Verb: " + method) }
			requestState = HEADERS
			break

		case HEADERS:
			if line == "\r\n" {
				if method == "GET" {
					requestState = COMPLETE
					break
				} else {
					requestState = BODY
					break
				}
			}
			headerParts := strings.Split(line, ":")
			headers[strings.TrimSpace(headerParts[0])] = strings.TrimSpace(headerParts[1])
		case BODY:
			body += line
		}
	}

	return path, nil
}

func getMethodAndPath(line string) (string, string, map[string]string, error){
	queryString := map[string]string{}
	tokens := strings.Split(line, " ")
	if len(tokens) != 3 {
		return "", "", nil,  errors.New("protocol error, expected METHOD, PATH, and VERSION")
	}
	pathParts := strings.Split(tokens[1], "?")
	if len(pathParts) > 1 {
		queryStringParts := strings.Split(pathParts[1], "&")
		for _, keyValuePair := range queryStringParts {
			keyValueParts := strings.Split(keyValuePair, "=")
			if len(keyValueParts) > 1 {
				queryString[keyValueParts[0]] = keyValueParts[1]
			}
		}
	}

	return tokens[0], pathParts[0], queryString, nil
}
