package hyperzelly

import (
	"fmt"
	"io/ioutil"
	"net"
)

func handleResponse(conn net.Conn, path string) {
	if path == "/" { path= "/index.html"}
	responseBody, _ := ioutil.ReadFile("/var/www" + path)

	response := fmt.Sprintf(`HTTP/1.1 200 OK
Server: HyperZelly
Content-Length: %d
Content-Type: text/html

%s`, len(responseBody), responseBody)
	_, _ = conn.Write([]byte(response))
}
